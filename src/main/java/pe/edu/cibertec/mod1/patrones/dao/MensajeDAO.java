/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.dao;

import java.util.List;
import pe.edu.cibertec.mod1.patrones.entidad.Mensaje;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
public interface MensajeDAO {
    /**
     * Graba un mensaje en la plataforma.
     *
     * Si el mensaje es nulo retorna false.
     *
     * @param mensaje
     * @return
     */
    boolean grabar(Mensaje mensaje);

    /**
     * Busca mensajes entre dos usuarios, si alguno es nulo retorna la lista
     * vacia.
     *
     * @param u1
     * @param u2
     * @return
     */
    List<Mensaje> buscar(Usuario u1, Usuario u2);

}
