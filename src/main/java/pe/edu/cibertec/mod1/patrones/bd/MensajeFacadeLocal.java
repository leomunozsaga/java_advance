/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.cibertec.mod1.patrones.bd;

import java.util.List;
import javax.ejb.Local;
import pe.edu.cibertec.mod1.patrones.entidad.Mensaje;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
@Local
public interface MensajeFacadeLocal {

    void create(Mensaje mensaje);

    void edit(Mensaje mensaje);

    void remove(Mensaje mensaje);

    Mensaje find(Object id);

    List<Mensaje> findAll();

    List<Mensaje> findRange(int[] range);

    List<Mensaje> findForUsuarios(Usuario u1, Usuario u2);

    int count();

}
