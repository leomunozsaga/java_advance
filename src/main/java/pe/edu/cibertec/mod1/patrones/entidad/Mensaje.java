/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.cibertec.mod1.patrones.entidad;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author JavaADV
 */
@Entity
public class Mensaje {


    @Id
    @GeneratedValue
    private Long id;
    private String mensaje;
    @ManyToOne
    private Usuario emisor;
    @ManyToOne
    private Usuario receptor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the emisor
     */
    public Usuario getEmisor() {
        return emisor;
    }

    /**
     * @param emisor the emisor to set
     */
    public void setEmisor(Usuario emisor) {
        this.emisor = emisor;
    }

    /**
     * @return the receptor
     */
    public Usuario getReceptor() {
        return receptor;
    }

    /**
     * @param receptor the receptor to set
     */
    public void setReceptor(Usuario receptor) {
        this.receptor = receptor;
    }


}
