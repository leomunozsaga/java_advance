package pe.edu.cibertec.mod1.patrones.bd;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pe.edu.cibertec.mod1.patrones.entidad.Mensaje;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;


/**
 *
 * @author JavaADV
 */
@Stateless
public class MensajeFacade extends AbstractFacade<Mensaje> implements MensajeFacadeLocal {

    @PersistenceContext(unitName = "mensajesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MensajeFacade() {
        super(Mensaje.class);
    }

    @Override
    public List<Mensaje> findForUsuarios(Usuario u1, Usuario u2) {
        final String query = "Select o from Mensaje o where (o.emisor = :u1 and o.receptor = :u2) or (o.emisor = :u2 and o.receptor = :u1) ORDER BY o.id";
        return em
                .createQuery(query, Mensaje.class)
                .setParameter("u1", u1)
                .setParameter("u2", u2)
                .getResultList();
    }

}
