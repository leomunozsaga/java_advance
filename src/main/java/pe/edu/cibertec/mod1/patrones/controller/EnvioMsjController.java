/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.controller;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import pe.edu.cibertec.mod1.patrones.entidad.Mensaje;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
// LEONIDAS MUÑOZ
@SessionScoped
public class EnvioMsjController implements Serializable{
    // los inject no requieren geter ni setter
    @Inject
    private Usuario uEmisor;
    private Usuario uReceptor;
    @Inject
    private Mensaje mensaje;
    private List<Mensaje> list_mensaje;
    
    
    public String ingresar(){
        return null;
    }
    
    public String enviar(){
        return null;
    }

    
    /**
     * @return the mensaje
     */
    public Mensaje getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the list_mensaje
     */
    public List<Mensaje> getList_mensaje() {
        return list_mensaje;
    }

    /**
     * @param list_mensaje the list_mensaje to set
     */
    public void setList_mensaje(List<Mensaje> list_mensaje) {
        this.list_mensaje = list_mensaje;
    }

    /**
     * @return the uEmisor
     */
    public Usuario getuEmisor() {
        return uEmisor;
    }

    /**
     * @param uEmisor the uEmisor to set
     */
    public void setuEmisor(Usuario uEmisor) {
        this.uEmisor = uEmisor;
    }

    /**
     * @return the uReceptor
     */
    public Usuario getuReceptor() {
        return uReceptor;
    }

    /**
     * @param uReceptor the uReceptor to set
     */
    public void setuReceptor(Usuario uReceptor) {
        this.uReceptor = uReceptor;
    }
    
}
