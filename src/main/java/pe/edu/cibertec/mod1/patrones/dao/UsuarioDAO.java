/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.cibertec.mod1.patrones.dao;

import java.util.List;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
public interface UsuarioDAO {

    /**
     * Graba un usuario en la base de datos.
     *
     * Si el usuario es nulo retorna false.
     *
     * @param u
     * @return
     */
    boolean grabar(Usuario u);

    /**
     * Busca un usuario por nombre.
     *
     * Si el nombre es nulo retorna nulo.
     *
     * @param nombre
     * @return
     */
    Usuario buscar(String nombre);
}
