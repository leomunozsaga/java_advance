/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.controller;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pe.edu.cibertec.mod1.patrones.dao.UsuarioBDDAO;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;
import pe.edu.cibertec.mod1.patrones.service.ServicioMensajes;

/**
 *
 * @author JavaADV
 */
@RequestScoped
@Named
public class MensajesController implements Serializable {
    // no nesecita getter y setter
    @Inject
    UsuarioBDDAO dao;
    @Inject
    ServicioMensajes servicioMensajes;

    private String usuario;
    private String destinatario;
    private String mensaje;
    private List<String> mensajes;

    public void ingresar() {
        buscarocrear(usuario);
    }

    public void enviar() {
        buscarocrear(destinatario);
        servicioMensajes.enviarMensaje(usuario, destinatario, mensaje);
        mensajes = servicioMensajes.obtenerMensajes(usuario, destinatario);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<String> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<String> mensajes) {
        this.mensajes = mensajes;
    }

    private void buscarocrear(String nombre) {
        Usuario u = dao.buscar(nombre);
        if (u == null) {
            u = new Usuario();
            u.setNombre(nombre);
            dao.grabar(u);
        }
    }

}
