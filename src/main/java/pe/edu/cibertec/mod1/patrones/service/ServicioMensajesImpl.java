/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import pe.edu.cibertec.mod1.patrones.dao.MensajeDAO;
import pe.edu.cibertec.mod1.patrones.dao.UsuarioDAO;
import pe.edu.cibertec.mod1.patrones.entidad.Mensaje;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
public class ServicioMensajesImpl implements ServicioMensajes{

    @Inject
    MensajeDAO mensajesDAO;

    @Inject
    UsuarioDAO usuarioDAO;

    @Override
    public void enviarMensaje(String emisor, String receptor, String mensaje) {
        if (emisor == null || receptor == null || mensaje == null) {
            throw new IllegalArgumentException();
        }
        Mensaje m = new Mensaje();
        Usuario e = usuarioDAO.buscar(emisor);
        Usuario r = usuarioDAO.buscar(receptor);
        m.setEmisor(e);
        m.setReceptor(r);
        m.setMensaje(mensaje);
        mensajesDAO.grabar(m);
    }

    @Override
    public List<String> obtenerMensajes(String usuario1, String usuario2) {
        if (usuario1 == null || usuario2 == null) {
            return Collections.emptyList();
        }
        Usuario u1 = usuarioDAO.buscar(usuario1);
        Usuario u2 = usuarioDAO.buscar(usuario2);
        final List<Mensaje> mensajes = mensajesDAO.buscar(u1, u2);
        final List<String> response = new ArrayList<>();
        for (Mensaje m : mensajes) {
            response.add(m.getMensaje());
        }
        return response;
    }
}
