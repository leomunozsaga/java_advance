/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.service;

import java.util.List;

/**
 *
 * @author JavaADV
 */
public interface ServicioMensajes {

    /**
     * Publica un mensaje
     *
     * Si alguno de los parametros es nulo o vacío emitir una excepcion
     * IllegalArgumentException.
     *
     * @param emisor quien emite el mensaje (no nulo)
     * @param receptor quien recibe el mensaje (no nulo)
     * @param mensaje el mensaje a enviar (no nulo)
     * @throws IllegalArgumentException por error en parametros
     */
    void enviarMensaje(String emisor, String receptor, String mensaje);

    /**
     * Obtiene la lista de mensajes entre dos usuarios.
     *
     * Si algun parametro es nulo o vacío se retorna una lista vacía.
     *
     * @param usuario1 primer usuario involucrado
     * @param usuario2 segundo usuario involucrado
     */
    List<String> obtenerMensajes(String usuario1, String usuario2);
}
