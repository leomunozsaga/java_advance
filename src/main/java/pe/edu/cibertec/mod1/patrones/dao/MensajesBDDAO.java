/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.dao;

import java.util.Collections;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import pe.edu.cibertec.mod1.patrones.bd.MensajeFacadeLocal;
import pe.edu.cibertec.mod1.patrones.entidad.Mensaje;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
@RequestScoped
public class MensajesBDDAO implements MensajeDAO{
    @Inject //obtener una referencia al servicio u objeto que requerimos
    MensajeFacadeLocal facade;

    @Override
    public boolean grabar(Mensaje mensaje) {
        if (mensaje == null) {
            return false;
        }
        facade.create(mensaje);
        return true;
    }

    @Override
    public List<Mensaje> buscar(Usuario u1, Usuario u2) {
        if (u1 == null || u2 == null) {
            return Collections.emptyList();
        }
        return facade.findForUsuarios(u1, u2);
    }

}
