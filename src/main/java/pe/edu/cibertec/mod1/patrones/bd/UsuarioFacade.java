/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.bd;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "mensajesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario findForNombre(String nombre) {
        final String query = "SELECT u from Usuario u WHERE u.nombre = :nombre";
        return em
                .createQuery(query, Usuario.class)
                .setParameter("nombre", nombre)
                .getSingleResult();
    }

}
