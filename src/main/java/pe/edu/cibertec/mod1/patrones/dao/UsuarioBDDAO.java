/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.edu.cibertec.mod1.patrones.dao;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import pe.edu.cibertec.mod1.patrones.bd.UsuarioFacadeLocal;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
@RequestScoped
public class UsuarioBDDAO implements UsuarioDAO {

    @Inject
    UsuarioFacadeLocal facade;

    @Override
    public boolean grabar(Usuario u) {
        if (u == null) {
            return false;
        }
        facade.create(u);
        return true;
    }

    @Override
    public Usuario buscar(String nombre) {
        if (nombre == null) {
            return null;
        }
        return facade.findForNombre(nombre);
    }

}
