/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.cibertec.mod1.patrones.bd;

import java.util.List;
import javax.ejb.Local;
import pe.edu.cibertec.mod1.patrones.entidad.Usuario;

/**
 *
 * @author JavaADV
 */
@Local
public interface UsuarioFacadeLocal {

    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    Usuario find(Object id);

    Usuario findForNombre(String nombre);

    List<Usuario> findAll();

    List<Usuario> findRange(int[] range);

    int count();

}
